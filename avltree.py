import copy


class Node:
    def __init__(self, key, left_tree, right_tree, parent, height):
        self.key = key
        self.left_child = left_tree
        self.right_child = right_tree
        self.parent = parent
        self.height = height

    @staticmethod
    def make_node(key):
        return Node(key, None, None, None, 0)

    @staticmethod
    def is_leaf(node):
        return (node.left_child is None) and (node.right_child is None)

    @staticmethod
    def have_children(node):
        return


class Tree:

    is_root_visited = False

    def __init__(self, node):
        self.root = node

    def __eq__(self, other):
        if self.root.key == other.root.key:
            return True
        else:
            return False

    @staticmethod
    def is_empty(tree):
        return tree is None

    @staticmethod
    def make_tree(key, left_tree = None, right_tree=None, parent=None, height=0):
        return Tree(Node(key, left_tree, right_tree, parent, height))

    @staticmethod
    def find(tree, key):
        if tree is not None:
            if key < tree.root.key:
                return Tree.find(tree.root.left_child, key)
            elif key > tree.root.key:
                return Tree.find(tree.root.right_child, key)
            return tree.root
        else:
            return None

    @staticmethod
    def insert(tree, key):
        if Tree.find(tree, key) is None:
            return Tree.recursive_insert(tree, key)
        else:
            return tree

    '''
    @staticmethod
    def recursive_insert(tree, key, parent = None):
        if Tree.is_empty(tree):
            return Tree.make_tree(key, None, None, None)
        else:
            if key < tree.root.key:
                return Tree.make_tree(tree.root.key, Tree.recursive_insert(tree.root.left_child, key, tree.root), tree.root.right_child, tree.root.parent)
            if key > tree.root.key:
                return Tree.make_tree(tree.root.key, tree.root.left_child, Tree.recursive_insert(tree.root.right_child, key), tree.root.parent)
    '''

    @staticmethod
    def recursive_insert(tree, key):
        if key < tree.root.key:
            if tree.root.left_child is None:
                tree.root.left_child = Tree.make_tree(key, None, None, None)
                tree.root.left_child.root.parent = tree
                tree.root.height = Tree.make_height(tree.root)
                if tree.root.parent is not None:
                    tree.root.parent.root.height = Tree.make_height(tree.root.parent.root)
                tree.root.left_child.root.parent = tree
                '''
                if tree.root.parent is not None:
                    tree.root.parent.root.left_child = Tree.make_balanced(tree)
                '''
            else:
                tree.root.left_child = Tree.recursive_insert(tree.root.left_child, key)
                tree.root.height = Tree.make_height(tree.root)
                if tree.root.parent is not None:
                    tree.root.parent.root.height = Tree.make_height(tree.root.parent.root)
                '''
                t = Tree.make_balanced(tree)
                if tree.root.parent is not None:
                    tree.root.parent.root.left_child = t
                    
                '''
        else:
            if tree.root.right_child is None:
                tree.root.right_child = Tree.make_tree(key, None, None, None)
                tree.root.right_child.root.parent = tree
                tree.root.height = Tree.make_height(tree.root)
                if tree.root.parent is not None:
                    tree.root.parent.root.height = Tree.make_height(tree.root.parent.root)
                tree.root.right_child.root.parent = tree
            else:
                tree.root.right_child = Tree.recursive_insert(tree.root.right_child, key)
                tree.root.height = Tree.make_height(tree.root)
                if tree.root.parent is not None:
                    tree.root.parent.root.height = Tree.make_height(tree.root.parent.root)
                '''
                if tree.root.parent is not None:
                    tree.root.parent.root.right_child = Tree.make_balanced(tree)
                '''
        return Tree.make_balanced(tree)
        #return tree

    @staticmethod
    def successor(tree, key):
        if tree is not None:
            node = Tree.find(tree, key)
            if node.right_child is not None:
                successor = node.right_child
                while successor.root.left_child is not None:
                    successor = successor.root.left_child
                return successor
            else:
                successor = node.parent
                while successor.root.parent is not None:
                    if successor.root.left_child is not None:
                        if successor.root.left_child.root == node:
                            break
                    node = copy.deepcopy(successor.root)
                    successor = node.root.parent
                return successor
        else:
            return None

    @staticmethod
    def successor_for_print(node):
        if tree is not None:
            if node.right_child is not None:
                successor = node.right_child
                while successor.root.left_child is not None:
                    successor = successor.root.left_child
                return successor
            else:
                successor = node.parent
                while successor.root.parent is not None:
                    if successor.root.left_child is not None:
                        if successor.root.left_child.root == node:
                            break
                    node = copy.deepcopy(successor.root)
                    successor = node.parent
                if successor.root.parent is None:
                    if tree.is_root_visited:
                        tree.is_root_visited = False
                        return None
                    else:
                        tree.is_root_visited = True
                return successor
        else:
            return None


    @staticmethod
    def successor_start(tree):
        while tree.root.left_child is not None:
            tree = tree.root.left_child
        return tree


    @staticmethod
    def successor_print(tree):
        tree = Tree.successor_start(tree)
        print(tree.root.key)
        temp = Tree.successor_for_print(tree.root)
        while temp is not None:
            temp_key = temp.root.key
            print(temp_key)
            tree = temp
            temp = Tree.successor_for_print(tree.root)

    @staticmethod
    def delete(tree, key):
        if Tree.find(tree, key) is not None:
            return Tree.recursive_delete(tree, key)
        else:
            return tree

    @staticmethod
    def recursive_delete(tree, key):
        if key == tree.root.key:
            if Node.is_leaf(tree.root):
                tree = None
            else:
                if tree.root.left_child is None:
                    parent = tree.root.parent
                    side = None
                    if parent is not None:
                        if parent.root.right_child == tree:
                            side = "right"
                    tree = tree.root.right_child
                    tree.root.parent = parent
                    if side is not None:
                        if side == "right":
                            tree.root.parent.root.right_child = tree
                        else:
                            tree.root.parent.root.left_child = tree
                    tree.root.height = Tree.make_height(tree.root)
                elif tree.root.right_child is None:
                    parent = tree.root.parent
                    side = None
                    if parent is not None:
                        if parent.root.right_child == tree:
                            side = "right"
                    tree = tree.root.left_child
                    tree.root.parent = parent
                    if side is not None:
                        if side == "right":
                            tree.root.parent.root.right_child = tree
                        else:
                            tree.root.parent.root.left_child = tree
                    tree.root.height = Tree.make_height(tree.root)
                else:
                    key = Tree.successor(tree, key).root.key
                    left_child = tree.root.left_child
                    right_child = tree.root.right_child
                    a = tree.root.parent
                    side = None
                    if a is not None:
                        if a.root.left_child == tree:
                            a.root.left_child = None
                            side = "l"
                        else:
                            a.root.right_child = None
                    tree = Tree.make_tree(key, left_child, Tree.delete(right_child, key), a, tree.root.height)
                    if side is not None:
                        if side == "l":
                            tree.root.parent.root.left_child = tree
                        else:
                            tree.root.parent.root.right_child = tree
                    tree.root.height = Tree.make_height(tree.root)
                    if tree.root.parent is not None:
                        tree.root.parent.root.height = Tree.make_height(tree.root.parent.root)
                    if tree.root.left_child is not None:
                        tree.root.left_child.root.height = Tree.make_height(tree.root.left_child.root)
                    if tree.root.right_child is not None:
                        tree.root.right_child.root.height = Tree.make_height(tree.root.right_child.root)
                    if tree.root.left_child is not None:
                        tree.root.left_child.root.parent = tree
                    if tree.root.right_child is not None:
                        tree.root.right_child.root.parent = tree
                        tree.root.right_child = Tree.make_balanced(tree.root.right_child)
                        tree.root.height = Tree.make_height(tree.root)
                    '''
                    if tree.root.left_child is not None:
                        tree.root.left_child.root.parent = root
                    if tree.root.right_child is not None:
                        tree.root.right_child.root.parent = root
                    '''
        else:
            if key < tree.root.key:
                #temp_tree = Tree.make_tree(tree.root.key, Tree.recursive_delete(tree.root.left_child, key), tree.root.right_child, tree.root.parent, 0) #Tree.change_h(tree.root))
                tree.root.left_child = tree.recursive_delete(tree.root.left_child, key)
                #parent = tree.root
                tree.root.height = Tree.make_height(tree.root)
                if tree.root.left_child is not None:
                    tree.root.left_child.root.parent = tree
            else:
               # temp_tree = Tree.make_tree(tree.root.key, tree.root.left_child, Tree.recursive_delete(tree.root.right_child, key), tree.root.parent, 0) #Tree.change_h(tree.root))
                tree.root.right_child = tree.recursive_delete(tree.root.right_child, key)
                #parent = tree.root
                tree.root.height = Tree.make_height(tree.root)
                if tree.root.right_child is not None:
                    tree.root.right_child.root.parent = tree
        return Tree.make_balanced(tree)

    @staticmethod
    def make_height(node):
        if node.right_child is None and node.left_child is None:
            return 0
        elif node.right_child is None:
            return node.left_child.root.height + 1
        elif node.left_child is None:
            return node.right_child.root.height + 1
        return max(node.left_child.root.height, node.right_child.root.height) + 1

    @staticmethod
    def small_left_rotate(tree):
        root = tree.root
        if root.right_child is None:
            return tree
        else:
            new_root = root.right_child
            parent = tree.root.parent
            side = ""
            if parent is not None:
                if parent.root.right_child is not None:
                    if parent.root.right_child == tree:
                        side = "right"
                    else:
                        side = 'left'
                else:
                    side = "left"
            left_tree = Tree.make_tree(root.key, root.left_child, new_root.root.left_child, new_root)
            left_tree.root.height = Tree.make_height(left_tree.root)
            if left_tree.root.parent is not None:
                left_tree.root.parent.root.height = Tree.make_height(left_tree.root.parent.root)
            temp = Tree.make_tree(new_root.root.key, left_tree, new_root.root.right_child)
            temp.root.height = Tree.make_height(temp.root)
            if temp.root.right_child is not None:
                temp.root.right_child.root.height = Tree.make_height(temp.root.right_child.root)
            if temp.root.left_child is not None:
                temp.root.left_child.root.height = Tree.make_height(temp.root.left_child.root)
            temp.root.parent = parent
            if parent is not None:
                if side == "right":
                    temp.root.parent.root.right_child = temp
                else:
                    temp.root.parent.root.left_child = temp
                temp.root.parent.root.height = Tree.make_height(temp.root.parent.root)
            if temp.root.left_child is not None:
                temp.root.left_child.root.parent = temp
            if temp.root.right_child is not None:
                temp.root.right_child.root.parent = temp
            return temp

    @staticmethod
    def small_right_rotate(tree):
        root = tree.root
        if root.left_child is None:
            return tree
        else:
            new_root = root.left_child
            parent = tree.root.parent
            side = ""
            if parent is not None:
                if parent.root.right_child == tree:
                    side = "right"
                else:
                    side = 'left'
            right_tree = Tree.make_tree(root.key, new_root.root.right_child, root.right_child, new_root)
            right_tree.root.height = Tree.make_height(right_tree.root)
            if right_tree.root.parent is not None:
                right_tree.root.parent.root.height = Tree.make_height(right_tree.root.parent.root)
            temp = Tree.make_tree(new_root.root.key, new_root.root.left_child, right_tree)
            temp.root.height = Tree.make_height(temp.root)
            if temp.root.left_child is not None:
                temp.root.left_child.root.height = Tree.make_height(temp.root.left_child.root)
            if temp.root.right_child is not None:
                temp.root.right_child.root.height = Tree.make_height(temp.root.right_child.root)
            temp.root.parent = parent
            if parent is not None:
                if side == "right":
                    temp.root.parent.root.right_child = temp
                else:
                    temp.root.parent.root.left_child = temp
                temp.root.parent.root.height = Tree.make_height(temp.root.parent.root)
            if temp.root.left_child is not None:
                temp.root.left_child.root.parent = temp
            if temp.root.right_child is not None:
                temp.root.right_child.root.parent = temp
            return temp

    @staticmethod
    def big_left_rotate(tree):
        root = tree.root
        if root.right_child is None:
            return tree
        else:
            rotated = Tree.make_tree(root.key, root.left_child, Tree.small_right_rotate(root.right_child), root.parent, root.height)
            rotated.root.height = Tree.make_height(rotated.root)
            #tree.root.height = Tree.make_height(tree.root)
            return Tree.small_left_rotate(rotated)

    @staticmethod
    def big_right_rotate(tree):
        root = tree.root
        if root.left_child is None:
            return tree
        else:
            rotated = Tree.make_tree(root.key, Tree.small_left_rotate(root.left_child), root.right_child, root.parent, root.height)
            rotated.root.height = Tree.make_height(rotated.root)
            #tree.root.height = Tree.make_height(tree.root)
            return Tree.small_right_rotate(rotated)

    @staticmethod
    def score(tree):
        if tree is not None:
            if tree.root.left_child is None and tree.root.right_child is None:
                return 0
            if tree.root.left_child is None:
                return -tree.root.right_child.root.height - 1
            elif tree.root.right_child is None:
                return tree.root.left_child.root.height+1
            else:
                a = tree.root.left_child.root.height - tree.root.right_child.root.height
                return a
        else:
            return 0

    @staticmethod
    def is_balanced(tree):
        return abs(Tree.score(tree)) <= 1

    @staticmethod
    def left_is_heavier(tree):
        return Tree.score(tree) > 1

    @staticmethod
    def right_is_heavier(tree):
        return Tree.score(tree) < 0

    @staticmethod
    def make_balanced(tree):
        if not Tree.is_balanced(tree):
            if Tree.left_is_heavier(tree):
                if Tree.right_is_heavier(tree.root.left_child):
                    return Tree.big_right_rotate(tree)  # в методичке правый
                else:
                    return Tree.small_right_rotate(tree)
            else:
                if Tree.left_is_heavier(tree.root.right_child):
                    return Tree.big_left_rotate(tree)     # в методичке левые
                else:
                    return Tree.small_left_rotate(tree)

        return tree

    @staticmethod
    def print(tree, tab_num = 0):
        if tree is None:
            return
        Tree.print(tree.root.right_child, tab_num + 1)
        print("    " * 2*tab_num + str(tree.root.key))
        Tree.print(tree.root.left_child, tab_num + 1)


tree = Tree.make_tree(12)
tree = Tree.insert(tree, 7)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 5)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 10)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 6)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 8)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 11)
Tree.print(tree)
print("----------------------")
tree = Tree.delete(tree, 7)
Tree.print(tree)
print("----------------------")
Tree.successor_print(tree)
'''
tree = Tree.insert(tree, 11)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 10)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 9)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 8)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 7)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 6)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 5)
Tree.print(tree)
print("----------------------")
tree = Tree.insert(tree, 4)
Tree.print(tree)
print("----------------------")
tree = Tree.delete(tree, 7)
Tree.print(tree)
print("----------------------")
tree = Tree.delete(tree, 11)
Tree.print(tree)
print("----------------------")
Tree.successor_print(tree)
'''
